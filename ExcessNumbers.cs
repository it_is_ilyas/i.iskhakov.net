﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public static class Extensions
{
    public static bool IsPrime(this int number)
    {
        if (number < 2) return false;
        for (int i = 2; i * i <= number; i++)
            if (number % i == 0)
                return false;
        return true;
    }

    public static bool IsExcessive(this int number)
    {
        int divsum = 0;
        if (number < 12) return false;
        for (int i = 1; i <= number / 2; i++)
        {
            if (number % i == 0)
            {
                divsum += i;
            }
        }

        if (divsum > number) return true;
        return false;
    }

    public static IEnumerable<T> Filter<T>(this IEnumerable<T> source, Func<T, bool> predicate)
    {
        foreach (var item in source)
        {
            if (predicate(item))
                yield return item;
        }
    }
}

public class Primes : IEnumerable<int>
{
    private class PrimeEnumerator : IEnumerator<int>
    {
        private int _current;
        public void Dispose() { }

        public bool MoveNext()
        {
            if (_current < int.MaxValue)
            {
                _current++;
                while (!_current.IsPrime())
                    _current++;

                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            _current = 0;
        }

        public int Current { get { return _current; } }

        object IEnumerator.Current
        {
            get { return Current; }
        }
    }

    private PrimeEnumerator _enumerator = new PrimeEnumerator();

    public IEnumerator<int> GetEnumerator()
    {
        return _enumerator;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}

public class Excess : IEnumerable<int>
{
    private class ExcessEnumerator : IEnumerator<int>
    {
        private int _current;
        public void Dispose() { }

        public bool MoveNext()
        {
            if (_current < int.MaxValue)
            {
                _current++;
                while (!_current.IsExcessive())
                    _current++;

                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            _current = 0;
        }

        public int Current { get { return _current; } }

        object IEnumerator.Current
        {
            get { return Current; }
        }
    }

    private ExcessEnumerator _enumerator = new ExcessEnumerator();

    public IEnumerator<int> GetEnumerator()
    {
        return _enumerator;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}

public class Program
{

    static IEnumerable<int> GetPrimes(int count = int.MaxValue)
    {
        yield return 2;
        var ctr = 1;
        for (var n = 3; ctr < count; n += 2)
            if (n.IsPrime())
            {
                ctr++;
                yield return n;
            }
        yield break;
    }

    static IEnumerable<int> GetExcesses(int count = int.MaxValue)
    {
        var ctr = 1;
        for (var n = 12; ctr < count; n++)
            if (n.IsExcessive())
            {
                ctr++;
                yield return n;
            }
        yield break;
    }



    static void Main(string[] args)
    {
        var numbers = GetExcesses().Take(10);//.Filter(x => x<100);//.Where(x => x.IsPrime());

        foreach (var n in numbers)
           Console.Write(n + " ");

        Console.ReadKey();
    }
}
